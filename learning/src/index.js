import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import {createStore} from 'redux';
import {Provider} from 'react-redux'
import allReducers from './components/Redux/reducer';

const mystore = createStore(allReducers)

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  // <React.StrictMode>
   <Provider store={mystore}>
    <App />
   </Provider>
  // </React.StrictMode>
);


reportWebVitals();
