import React, { useState } from "react";
import ChildComponent from "./child";

const ParentComponent = () => {
    const[state,setstate]=useState("Parent Component")
    const changeName = (p) =>{
        setstate(p)
    }

    return (
        <>
        <h4>{state}</h4>
        <ChildComponent
        name="gehan"
        email="gehan@gmail.com"
        country="Srilanka"
        num1="100"
        num2={200}
        num3={400}
        changeName={changeName}/>
        </>)
}
export default ParentComponent