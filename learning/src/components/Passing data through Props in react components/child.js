import React, { useState } from "react";

const ChildComponent = (prop) => {
    const changeParentClassName = prop.changeName
    const [childTopics, setChildTopics] = useState('Child Component')
    const val = prop.num2 + prop.num3
    return (
        <>
            <div className="container">
                <div class="card">
                    <div class="card-header">
                        <h5>{childTopics}</h5>
                    </div>
                    <div class="card-body">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">{prop.name}</li>
                            <li class="list-group-item">{prop.email}</li>
                            <li class="list-group-item">{prop.country}</li>
                            <li class="list-group-item">num2:{prop.num2}  num3: {prop.num3}={val}</li>
                            <li class="list-group-item">   <button onClick={() => setChildTopics('updated name')} >change name</button><button onClick={() => setChildTopics('Child Component')} >first name</button></li>
                            <li class="list-group-item">   <button onClick={() => changeParentClassName('updated name')} >change parent component name</button><button onClick={() => changeParentClassName('Parent Component')} >change parent component first name</button></li>
                        </ul>
                    </div>
                </div>
            </div>


        </>)
}
export default ChildComponent