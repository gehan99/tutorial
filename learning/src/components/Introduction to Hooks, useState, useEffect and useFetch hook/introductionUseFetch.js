import React, { useEffect, useState } from "react";
import Axios  from "axios";

const UseFetchComponent = ()=>{
    const url ="https://jsonplaceholder.typicode.com/todos/1"
    const[data,setdata]=useState('')
    const[loading,setLoading]=useState(false)
    const[error,seterror]=useState(null)
    useEffect(()=>{

        setLoading(true)
        Axios.get(url).then((res)=>{
            setdata(res.data)
            console.log(res.data)
        }).catch((err)=>{
            seterror(err)}).finally(()=>
            {
            setLoading(false)
        })
        

    })

    // if(loading)return <h3>LOADING...</h3>
    // if(error)return <h4>err:{error}</h4>
    return(
    <>
    Dataset :{console.log("data:",data.userId)}
    userId:{data.id}
        </>)
    // {data,loading,error}
}

export default UseFetchComponent