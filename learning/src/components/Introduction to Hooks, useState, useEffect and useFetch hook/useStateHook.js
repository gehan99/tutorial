import React, { useState } from "react";

const UseStateComponent = () => {
    const [numVal, setNumVal] = useState(0)
    const [color, setColor] = useState("red")
    const [bbjdata, setObjData] = useState({
        brand: "Ford",
        model: "Mustang",
        year: "1964",
        color: "red"
    })
    return (
        <>
            <p>value:{numVal}</p>
            <button onClick={() => setNumVal(numVal + 1)}>click</button>
            <button onClick={() => setNumVal(0)}>Reset</button>
            <br />
            <p>color:{color}</p>
            <button onClick={() => setColor("green")}>click</button>
            <button onClick={() => setColor("red")}>Reset</button>

            <br /> <br />
            <p>brand:{bbjdata.brand}</p>
            <p>model:{bbjdata.model}</p>
            <p>year:{bbjdata.year}</p>
            <p>color:{bbjdata.color}</p>
            <button onClick={() => setObjData(previousState => {
                return { ...previousState, color: "blue" ,year:"2000"}
            })}>click</button>
            <button onClick={() => setObjData(previousState => {
                return { ...previousState, color: "red" ,year:"1994"}
            })}>Reset</button>
        </>
    )
}
export default UseStateComponent