import React, { useEffect, useState } from "react";

const UseEffectComponent = () => {
    const [count, setCount] = useState(0);
    useEffect(() => {
        setTimeout(() => {
            setCount((count) => count + 1);
        }, 1000);
    }, [count]);
    return (
        <>
            <div>
                hello
                <h7>I've rendered {count} times!</h7>
            </div>

            <li>No dependency passed</li>
            <p>Runs on every render</p>
            <li>An empty array</li>
            <p>Runs only on the first render</p>
            <li>Props or state values</li>
            <p>Runs on the first render</p>
            <p>And any time any dependency value changes</p>
        </>
    )
}

export default UseEffectComponent