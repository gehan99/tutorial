import React, { Component } from "react";
import ComponentUpdatedata from "./ComponentUpdateData";
import GetDerivedStateFromPropsData from "./getDerivedStateFromPropsData";
import ShouldComponentUpdateData from "./ShouldComponentUpdateData";

class ComponenedDidmountData extends Component {

    constructor(props) {
        super(props)
        this.state = {
            count: 0
        }
        this.increeseVal = () => { this.setState({ count: this.state.count + 1 }) }
        this.decreseVal = () => {
            if (this.state.count > 0) this.setState({ count: this.state.count - 1 })
        }
    }
    componentDidMount() {
        // console.log("component didmount")
        // console.log("-------")
    }
    render() {
        return (
            <>
                <h4>ComponenedDidmountData</h4>
                <div className="constainer">
                    <div className="row">
                        <div className=" col-2" style={{ padding: "1%" }}><button style={{ backgroundColor: "red" }} onClick={this.increeseVal} >increase +</button></div>
                        <div className=" col-2" style={{ padding: "1%" }}>{this.state.count}</div>
                        <div className="col-2" style={{ padding: "1%" }}><button style={{ backgroundColor: "green" }} onClick={this.decreseVal}>decrese -</button></div>
                    </div>
                </div>
                <ComponentUpdatedata
                data={this.state.count}
                />
                <GetDerivedStateFromPropsData
                data={this.state.count}/>
                <ShouldComponentUpdateData/>
            </>
        )
    }


    componentDidUpdate(pProps,pState,pSnapShot) {
        // console.log("component didupdate")
        // console.log("-------")
        // console.log("privious props  =",pProps)
        // console.log("privious pState  =",pState)
        // console.log("privious pSnapShot  =",pSnapShot)
    }
    

    componentWillUnmount() {
        console.warn("component willunmount")
        // console.log("-------")
    }
}

export default ComponenedDidmountData