import React, { Component } from "react";

class ShouldComponentUpdateData extends Component {
    constructor() {
        super()
        this.state = {
            count: 0
        }
    }
    shouldComponentUpdate() {
        console.warn("ShouldComponentUpdate==",this.state.count)
        if(this.state.count<97){
            return true
        }
    }
    render() {
        return (
            <>
                <h6>ShouldComponentUpdateData  - {this.state.count}</h6>
                <div>
                    <button onClick={() => this.setState({ count: this.state.count + 7 })}>ShouldComponentUpdateData *7</button>
                </div>
            </>
        )
    }

}

export default ShouldComponentUpdateData