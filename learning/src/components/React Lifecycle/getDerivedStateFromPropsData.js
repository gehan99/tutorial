import React, { Component } from "react";

class GetDerivedStateFromPropsData extends Component {
    constructor(){
        super()
        this.state={
            currantVal:0
        }
    }

   static getDerivedStateFromProps(props,state){
        console.log("GetDerivedStateFromPropsData props ++++----------------",props,state)
        console.log("GetDerivedStateFromPropsData state ++++",state)
        return {
            currantVal:props.data + 100
        }

    }
    render() {
        return (
            <>
            <h6>GetDerivedStateFromPropsData:{this.state.currantVal}</h6></>
        )
    }
}

export default GetDerivedStateFromPropsData