import React, { Component } from "react";
import MountingDataNew from "./mountNew";
import ComponenedDidmountData from "./ComponentdidmoutData";

class MountingData extends Component{
    constructor(props){
        super(props)
        this.state={
            title:"class component",
            mount:true,
            ignoreProp:0
        }
        console.log("construtor")

        this.mount=()=>{this.setState({mount:true})}
        this.unmount=()=>{this.setState({mount:false})}

        this.ignorProps=()=>{this.setState({ignoreProp:100})}
    }


    render(){
        console.log("render")
        return(
            <>
            <h5>{this.state.title}</h5>
            <MountingDataNew/>
            <button onClick={this.mount} disabled={this.state.mount}>mount</button>
            <button onClick={this.unmount} disabled={!this.state.mount}>unmount</button>
            {this.state.mount?<ComponenedDidmountData/>:"click mount button"}
            
            </>
        )
    }


}

export default MountingData