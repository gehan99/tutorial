import React,{useContext, useEffect} from "react";
import { UserContext } from "./hokksData";


const ContextData = ()=>{
    const data = useContext(UserContext)
    useEffect(()=>{
        console.log("contextdata",data)
    })
    return(
        <>
        <h6>context data:{data}</h6>
        </>
    )
}
export default ContextData