import React, { useEffect, useState }  from "react";
import Axios  from "axios";


const ErrorsDataHandle = ()=>{
    const url = "https://jsonplaceholder.typicode.com/todos"
    const[error,seterror]=useState('')
    const[todos,setTodos]=useState([])
    useEffect(()=>{
        console.log("ddd")
        Axios.get(url).then((res)=>{
            console.log("res",res.data)
            setTodos(res.data)
        }).catch((err)=>{
            seterror(err)
            console.log("error",err)
        })
    })
    return(
        <>
        <p>Data set :{todos.map((i,j)=>{
            return(
                <h6 className="container">{todos[j].userId} {todos[j].title} </h6>
            )
        })}</p>
        </>
    )
}
export default ErrorsDataHandle