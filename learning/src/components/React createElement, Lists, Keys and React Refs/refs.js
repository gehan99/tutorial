import React, { useEffect, useRef } from "react";

const RefsComponent = () => {
    const First_Name = useRef(null)
    const Last_Name = useRef(null)
    const submitbutton = useRef(null)
    useEffect(()=>{
        First_Name.current.focus()
    })

    return (
        <>
            <h5>RefsComponent</h5>
            <div className="container">
                <div>
                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">First Name</label>
                        <input type="text" className="form-control" ref={First_Name} aria-describedby="emailHelp" placeholder="Enter email" />

                    </div>
                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Last Name</label>
                        <input type="text" className="form-control" ref={Last_Name} placeholder="Password" />
                    </div>
                    <div className="form-check">

                    </div>
                    <button  className="btn btn-primary" ref={submitbutton}>Submit</button>
                </div>
            </div>
            {/* https://www.youtube.com/watch?v=ScT4ElKd6eo */}

        </>
    )
}

export default RefsComponent