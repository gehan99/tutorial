import React from "react";
import ReactElementComponent from "../React createElement, Lists, Keys and React Refs/reacteliment";
import ListComponent from "../React createElement, Lists, Keys and React Refs/list";
import RefsComponent from "../React createElement, Lists, Keys and React Refs/refs";
const ElemetsComponent = ()=>{
    return(
        <>
        <ReactElementComponent/>
        <ListComponent/>
        <RefsComponent/>
        </>
    )
}
export default ElemetsComponent