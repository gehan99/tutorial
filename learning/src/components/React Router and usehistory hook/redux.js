import React from "react";
import {useSelector,useDispatch} from 'react-redux'
import {increment,decrement,login,logout} from '../Redux/actions'

const CreateReduxComponent = ()=>{
    const counter = useSelector(state=>state.counter)
    const isLogged = useSelector(state=>state.isLogged)
    const dispatch = useDispatch();
    return(
        <>
        <h4>redux</h4>
        <p>Counter:{counter} </p>
        <button onClick={()=>dispatch(increment())}>increase +</button>
        <button onClick={()=>dispatch({type:'DECREMENT'})}>decrement -</button>
       
        <p>LoginStates  :{isLogged?"hello":"please logging"} </p>
        {isLogged?<><button onClick={()=>dispatch(logout())}>logout</button> </>:<><button onClick={()=>dispatch(login())}>login</button></>}
       
        
        </>
    )
}
export default CreateReduxComponent