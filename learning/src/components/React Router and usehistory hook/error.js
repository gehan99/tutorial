import React, { createContext, useEffect, useState } from "react";
import ErrorsDataHandle from "../Context Provider and Error handling/ErrorHandle";
import ContextData from "../Context Provider and Error handling/contextprovider";
import Axios from "axios";
import { UsersProvider } from "../Context Provider and Error handling/hokksData";

const MyData = createContext() 
const ErrorHandle = () => {
    const[state,setState]=useState([])

    const url = "https://jsonplaceholder.typicode.com/todos"
   
    useEffect(() => {
        Axios.get(url).then((res) => {
            console.log("responce", res)
            setState(res.data)
        }).catch((err) => {
            alert("error: ", err)
        })
    })
    return (
        <>
            <h3>Error handle</h3>
            <ErrorsDataHandle />
            
            <UsersProvider>
                <ContextData />
            </UsersProvider>
        </>
    )
}

export default ErrorHandle

