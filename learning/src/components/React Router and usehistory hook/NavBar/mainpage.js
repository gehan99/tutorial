import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import NavigationBar from "./navigationBar";
import Homepage from "../home";
import Page1 from "../page1";
import Page2 from "../page2";
import Page3 from "../page3";
import PageHooks from "../pageHooks";
import ElemetsComponent from "../element";
import ErrorHandle from "../error";
import CreateReduxComponent from "../redux";

const Mainpage = ()=>{
    return(
        <>
        <BrowserRouter>
        <NavigationBar/>
        <Routes>
            <Route path="/" element={<Homepage/>}></Route>
            <Route path="/page1" element={<Page1/>}></Route>
            <Route path="/page2" element={<Page2/>}></Route>
            <Route path="/page3" element={<Page3/>}></Route>
            <Route path="/pageHooks" element={<PageHooks/>}></Route>
            <Route path="/element" element={<ElemetsComponent/>}></Route>
            <Route path="/error" element={<ErrorHandle/>}></Route>
            <Route path="/redux" element={<CreateReduxComponent/>}></Route>
        </Routes>
        </BrowserRouter>
        </>
    )
}

export default Mainpage