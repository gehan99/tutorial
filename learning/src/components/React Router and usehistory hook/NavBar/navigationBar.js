import React,{useEffect}from "react";
import {Link} from 'react-router-dom'
import {useSelector,useDispatch} from 'react-redux'




const NavigationBar = ()=>{
  const isLogged = useSelector(state=>state.isLogged)
  const dispatch = useDispatch();

  useEffect(()=>{

  })

    return(
        <>
        <div>
        <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
          <a className="navbar-brand">Navbar</a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
              <li className="nav-item active">
              <Link className="nav-link" aria-current="page" to='/' >Home</Link>
              </li>
              <li className="nav-item">
              <Link className="nav-link" aria-current="page" to='/page1' >Page1</Link>
              </li>
              <li className="nav-item">
              <Link className="nav-link" aria-current="page" to='/page2' >Page2</Link>
              </li>
              <li className="nav-item">
              <Link className="nav-link" aria-current="page" to='/page3' >Page3</Link>
              </li>
              <li className="nav-item">
              <Link className="nav-link" aria-current="page" to='/pageHooks' >Hooks</Link>
              </li>
              <li className="nav-item">
              <Link className="nav-link" aria-current="page" to='/element' >Elements</Link>
              </li>
              <li className="nav-item">
              <Link className="nav-link" aria-current="page" to='/error' >Error</Link>
              </li>
              <li className="nav-item">
              <Link className="nav-link" aria-current="page" to='/redux' >Redux</Link>
              </li>
              {isLogged?<><li className="nav-item">
              <Link className="nav-link" aria-current="page" to='/redux' >Redux</Link>
              </li></>:<><p></p></>}
              
            </ul>
          </div>
        </nav>
      </div>
        </>
    )
}

export default NavigationBar