import React from "react";
import UseStateComponent from "../Introduction to Hooks, useState, useEffect and useFetch hook/useStateHook";
import UseEffectComponent from "../Introduction to Hooks, useState, useEffect and useFetch hook/introductionUseEffect";
import UseFetchComponent from "../Introduction to Hooks, useState, useEffect and useFetch hook/introductionUseFetch";
const PageHooks = () => {
    // const{data,loading,error}= UseFetchComponent("https://jsonplaceholder.typicode.com/todos/1")
    // if(loading)return<h1>LOADING....</h1>
    // if(error)console.log(error)
    return (
        <>
            <h6>Introduction Hooks</h6>
            <div className="container">
                <p>Hooks allow function components to have access to state and other React features. Because of this, class components are generally no longer needed.</p>
                <p>Hooks allow us to "hook" into React features such as state and lifecycle methods.</p>
                <h5>Hook Rules</h5>
                <li>Hooks can only be called inside React function components.</li>
                <li>Hooks can only be called at the top level of a component.</li>
                <li>Hooks cannot be conditional</li>
                <br/>
                <h5>UseState</h5>
                <UseStateComponent/>
                <br/> <br/>
                <h5>UseEffect</h5>
                <UseEffectComponent/>
                <br/> <br/>
                <h5>UseFetchData</h5>
                <UseFetchComponent/>
                {/* <p>Data:{data}</p> */}
            </div>

        </>

    )
}
export default PageHooks