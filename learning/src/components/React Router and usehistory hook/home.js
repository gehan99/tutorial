import React from "react";
import { useNavigate} from 'react-router-dom'

const Homepage = () =>{
    const nextPage = useNavigate()
    const GotoNextpage = ()=>{
        nextPage('/page1')
    }
    return(
        <>
        <h6>home page</h6>
        <button onClick={GotoNextpage}>Goto Page1</button>
        <button >Goto Page2</button>
        
        </>
    )
}

export default Homepage