import React, { useState } from "react";
import ElementData from "./element";
import Pagescomponent from "./priventing";


const ConditionalRendering = () =>{
    const samplevariable1 = 0
    const samplevariable2 = true
    const samplevariable3 = "true"

    const[samplevariable4,setSamplevariable4]=useState(true)
    const[samplevariable5,setSamplevariable5]=useState(true)
    const[sampleMappingData,setSampleMappingData]=useState(['gehan','gayan','rave','mahesh'])

    const checkSample =()=>{
        setSamplevariable5(!samplevariable5)
    }
    return(
        <>
        
        <div className="container">
        <h6>ConditionalRendering</h6>
            {/* variable */}
            
           
            <div className="row">
                <div className="col">samplevariable1 : {samplevariable1}</div>
            </div>
            <div className="row">
                <div className="col">samplevariable2 :  {samplevariable2}</div>
            </div>
            <div className="row">
                <div className="col">samplevariable3 :  {samplevariable3}</div>
            </div>
            <br/>
            <h6>Inline If with Logical && Operator</h6>
            <div className="row">
                <div className="col">samplevariable4 :  {samplevariable4 && <p>hello</p>}</div>
            </div>
            <div className="row">
                <div className="col">samplevariable5 : {samplevariable5 && <p>check : Inline If-Else with Conditional Operator</p>}</div>
                <h6>Inline If-Else with Conditional Operator</h6>
                <div className="col"><button onClick={checkSample}>{samplevariable5 ?'unclick':'click'}</button></div>
            </div>
            <h6>Mapping Data</h6>
            <div className="row">
                <div className="col">{
                   sampleMappingData.map(i=><p style={{color:"red"}}>{i}</p>) 
                }</div>
            </div>

            <h6>Elements variables</h6>
            <div className="row">
                <ElementData/>
            </div><br/>
            <h6>Preventing Component from Rendering</h6>
            <div className="row">
                <div className="col">
                    <Pagescomponent/>
                </div>
            </div>
        </div>
        
        </>
    )
}
export default ConditionalRendering