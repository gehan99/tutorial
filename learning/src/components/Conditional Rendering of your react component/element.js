import React from "react";
import ReactDOM from 'react-dom/client';


const ElementData = (props)=>{
    if(props.isLoggedIn === false){
        return <UserGreeting/>
    }else{
        return<GuestGreeting/>
    }
}




const UserGreeting =()=>{
    return <h7>welcome back!</h7>
}
const GuestGreeting =()=>{
    return <h7>Please sign up.</h7>
}




const root = ReactDOM.createRoot(document.getElementById('root')); 
root.render(<ElementData isLoggedIn={true} />);

export default ElementData