import React from "react";
import { useForm } from "react-hook-form"
import { yupResolver } from "@hookform/resolvers/yup"
import * as yup from "yup"

const useSchema = yup.object().shape({
    FirstName: yup.string().required("required"),
    lastname: yup.string().required("required"),
    address: yup.string().required("required"),
    email: yup.string().email("invalid email").required("required"),
})


const useSchema2 = yup.object().shape({
    FirstName: yup.string().required("required"),
    email: yup.string().email("invalid").required("required"),

})

const createUser = async (event) => {
    event.preventDefault()
    let formData = {
        FirstName: event.target[0].value,
        lastname: event.target[1].value,
        address: event.target[2].value,
        email: event.target[3].value,
    };
    console.log("formData:", formData)
    const isValid = await useSchema.isValid(formData)
    console.log(isValid)
}





const RegisterPage = () => {
    const { register, handleSubmit, formState: { errors } } =
        useForm(
            { resolver: yupResolver(useSchema2) }
        )

    const submitForm = (data) => {
        console.log(data)
    }
    return (
        <>
            <form onSubmit={createUser}>
                <div className="mb-3">
                    <label htmlFor="name" className="form-label">FirstName</label>
                    <input
                        type="text"
                        className="form-control"
                        id="FirstName"
                        aria-describedby="nameHelp"
                    />

                </div>
                <div className="mb-3">
                    <label htmlFor="lastname" className="form-label">LastName</label>
                    <input type="text" className="form-control" id="LastName" aria-describedby="nameHelp" />
                </div>
                <div className="mb-3">
                    <label htmlFor="address" className="form-label">Address</label>
                    <input type="text" className="form-control" id="Address" aria-describedby="nameHelp" />
                </div>
                <div className="mb-3">
                    <label htmlFor="exampleInputEmail" className="form-label">Email</label>
                    <input type="email" className="form-control" id="Email" />
                </div>

                <button type="submit" className="btn btn-primary">Submit</button>
            </form>
            <br />
            <div>




                <form onSubmit={handleSubmit(submitForm)}>
                    <div className="mb-3">
                        <label htmlFor="name" className="form-label">FirstName</label>
                        <input
                            type="text"
                            className="form-control"
                            name="FirstName"
                            {...register("FirstName")}
                            aria-describedby="nameHelp"
                        />
                        <p>{errors.FirstName?.message}</p>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="exampleInputEmail" className="form-label">Email</label>
                        <input type="email" className="form-control" id="Email" {...register("email")} />
                        <p>{errors.email?.message}</p>
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        </>
    )
}

export default RegisterPage