export const increment = (a)=>{
    return{
        type:'INCREMENT'
    }
}
export const decrement = ()=>{
    return{
        type:'DECREMENT'
    }
}

export const login = ()=>{
    return{
        type:'SIGNIN'
    }
}

export const logout = ()=>{
    return{
        type:'SIGNOUT'
    }
}