import counterReducer from "./counter";
import isLoggedreducer from "./isLogged";
import {combineReducers} from 'redux';

const allReducers = combineReducers({
    counter: counterReducer,
    isLogged: isLoggedreducer
})
export default allReducers

