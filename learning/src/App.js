import './App.css';
// import ParentComponent from './components/Passing data through Props in react components/parent';
import MountingData from './components/React Lifecycle/mounting';
// import MountingDataNew from './components/React Lifecycle/mountNew';
// import MainPage from './components/main';
// import ConditionalRendering from './components/Conditional Rendering of your react component/conditionalRendering_1';
import Mainpage from './components/React Router and usehistory hook/NavBar/mainpage';

function App() {
  return (
    // <div className="App">
    <div>
      {/* <ParentComponent/> */ }
  {/* <MountingData/> */ }
  {/* <MountingDataNew/> */ }
  {/* <ConditionalRendering /> */}
  <Mainpage/>
    </div >
  );
}

export default App;
