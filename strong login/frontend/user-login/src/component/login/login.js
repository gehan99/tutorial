import React from "react";
import './login.css';

const LoginPage = () => {
    return (
        <>
            login component
            <div style={{ padding: 10 }}>
                <button type="button" class="btn btn-primary">Primary</button>
            </div>
            <br />

        <div className="container">
            <div className="row bg-light p-3">
                <div className="col-sm-2 col-md-5 col-lg-2 offset-2 ">col one</div>
                <div className="col-sm-3 col-md-7 col-lg-6 offset-2">col two</div>
            </div>
            <div className="row">
                <div className="col ">col one-2</div>
                <div className="col">col two-2</div>
            </div>

            <div className="row">
                <div className="col-sm-4 col-md-5 col-lg-6">
                    <div className="p-3 bg-light border">col one-3</div>
                </div>
                <div className="col-sm-4 col-md-5 col-lg-6">
                <div className="p-3 bg-light border">col two-3</div>
                </div>
            </div>
            <br/>
            <div className="row my-custom-row justify-content-end align-items-end">
                <div className="col-sm-4"><div className="p-3 bg-light border">col one-3</div></div>
                <div className="col-sm-4"><div className="p-3 bg-light border">col one-3</div></div>
            </div>

            <div className="row">
                <div className="col-sm-4">hello</div>
            </div>

            <div className="container">
                <div className="row">
                    <div className="col-sm p-3 bg-light border">1</div>
                    <div className="col-sm p-3 bg-light border">2</div>
                    <div className="col-sm p-3 bg-light border">3</div>
                </div>
                <div className="row gx-3">
                    <div className="col-sm p-3 bg-light border">1</div>
                    <div className="col-sm p-3 bg-light border">2</div>
                </div>
                <div className="row">
                    <div className="col-sm p-3 bg-light border">1</div>
                </div>
                <div className="row">
                    <div className="col-sm p-3 bg-light border">1</div>
                </div>
            </div>

        </div>


        </>
    )
}

export default LoginPage